This repository contains optional recycler-configurations for Snacks! (created by [tgruetzm](https://github.com/tgruetzm) continued by [Angel-125](https://github.com/Angel-125)).

## Dependencies
* KSP 1.4.x - 1.11.x
* latest Snacks! versions: [KSP Forum](http://forum.kerbalspaceprogram.com/index.php?/topic/149604-12-snacks-continued-v160-friendly-simplified-life-support/) [GitHub](https://github.com/Angel-125/Snacks)

## modified parts:
* Stock
  * ISRU (2.5m & 1.25m) gets SoilComposter modules (needs MKS/USI-LS)
* [Snacks](http://forum.kerbalspaceprogram.com/index.php?/topic/149604-12-snacks-continued-v160-friendly-simplified-life-support/) by created by [tgruetzm](https://github.com/tgruetzm) continued by [Angel-125](https://github.com/Angel-125)
  * Radial Soil Recycler Tin (new part based on radialSnackTin) has a SoilRecycler module
* [Modular Kolonization System (MKS)](https://github.com/UmbraSpaceIndustries/MKS) by RoverDude
  * Tundra RecyclingPlant gets a SoilRecycler module
  * MKS 'Atlas' Kerbitat (10m/20m) gets a SoilRecycler module
  * MKS 'Atlas' Agriculture Module (10m/20m) gets a SoilRecycler module
  * MKS 'Atlas' Factory (10m/20m) gets a SoilRecycler module
* [Near Future Spacecraft](https://github.com/ChrisAdderley/NearFutureSpacecraft) by ChrisAdderley
  * PPD-24 Itinerant Service Container gets a SoilRecycler module
* [Kerbal Planetary Base Systems](https://github.com/Nils277/KerbalPlanetaryBaseSystems) by Nils277
  * K&K Planetary Greenhouse gets a SoilRecycler module
  * K&K Greenhouse Container gets a SoilRecycler module
* [Stockalike Mining Extension](https://github.com/SuicidalInsanity/Stockalike-Mining-Extension) by SuicidalInsanity
  * SMX_Size0ISRU gets SoilComposter modules (needs MKS/USI-LS)
  * SMX_Size3ISRU gets SoilComposter modules (needs MKS/USI-LS)
* [XKOM](http://forum.kerbalspaceprogram.com/index.php?/topic/132820-wip113-xkom-interceptors-update-the-raven-takes-flight) by njmksr
  * Skyranger Fuselage gets a SoilRecycler module

## Side-Notes
* First of all, these configs are free to use!
* I hope that these modifications are reasonably improving your gameplay.
* AutoShutdown = true just to make sure snacks are not wasted into space or on the celestial body :)
* K&K Greenhouse Container has no storage of snacks or soil so make sure you have these containers/parts
* finetuned standard rates for SoilRecycler modules (100% efficiency set) --> 2 Snacks/Soil per seat per day

## MKS & USI-LS (SoilComposter modules)
* SoilComposter [Soil --> Dirt] becomes available if you use [Modular Kolonization System (MKS)](https://github.com/UmbraSpaceIndustries/MKS) by RoverDude
* SoilComposter [Soil --> Mulch] becomes available if you use [USI Life Support (USI-LS)](https://github.com/UmbraSpaceIndustries/USI-LS) by RoverDude
* SoilComposter [Soil --> Mulch+Dirt] becomes available if you use both MKS & USI-LS

Feedback is very welcome, happy soil converting! :)

## ToDo
- [x] more Koffee!
- [x] more parts (eventually)
- [X] more recycling-fun!

